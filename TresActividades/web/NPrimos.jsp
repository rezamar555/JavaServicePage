<%-- 
    Document   : NPrimos
    Created on : 22/02/2018, 03:24:50 AM
    Author     : mareyes
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="shortcut icon" href="ico.ico">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/Style.css">
        <!--<link rel="stylesheet" href="fontawesome-free-5.0.6/web-fonts-with-css/CSS/fontawesome.min.css"> -->
        <script src="js/jquery-3.3.1.js"></script>
        <script src="js/bootstrap.min.js"></script>
        
        <nav class="navbar navbar-default" role="navigation">
            <div class="navigation-header">                
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">                            
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                    </button>  
                <a class="navbar-brand" href="https://www.gitlab.com/Rezamar"><img id="logo" src="mareyes.png" id="logo"></a>
            </div>
            
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-left">
                    <li><a href="NPrimos.jsp">Números primos</a></li>
                    <li><a href="Formulario.jsp">Formulario</a></li>
                    <li><a href="Figu.jsp">Cálculo de Figuras</a></li>  
                    <li><a href="Formulario2.jsp">Formulario2</a></li>
                    <li><a href="refaccionaria.jsp">Refaccionaria</a></li>
                </ul>
            </div>
        </nav>
        
    </head>
    <body>        
        <form action="Ejecutor.jsp">      
            <h3>Descripción: Calcule los numeros primos entre un rango de numeros a y b,<br> donde a < b
                los resultados se mostrarán en formato de tabla en html.</h3>
            <br>
            Ingrese el primer numero:<br>
                <input type="text" name="uno"><br><br>
                Ingrese el segundo numero:<br>
                <input type="text" name="dos">
                <br><br>
                <input type="submit" name="enviar" value="Enviar">
        </form>                
    </body>
</html>
